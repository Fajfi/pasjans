#include <iostream>
#include "karta.h"
#include <ctime>
#include <string>
using namespace std;


void Karta::WstawNaPoczatek(Karta* &Head, Karta * &Tail, int id, int wartosc, string kolor, string znak)
{
	Karta * nowy = new Karta();

	nowy->id_karty = id;
	nowy->kolor_karty = kolor;
	nowy->wartosc_karty = wartosc;
	nowy->znak_karty = znak;
	

	if (Head == nullptr)
	{
		nowy->Next = nullptr;
		nowy->Prev = nullptr;
		Tail = nowy;

	}
	else
	{
		Head->Prev = nowy;
		nowy->Next = Head;
	}
	Head = nowy;

}

void Karta::WstawNaKoniec(Karta* &Tail, int id, int wartosc, string kolor, string znak)
{
	Karta * nowy = new Karta();
	nowy->id_karty = id;
	nowy->kolor_karty = kolor;
	nowy->wartosc_karty = wartosc;
	nowy->znak_karty = znak;

	Tail->Next = nowy;
	nowy->Prev = Tail;
	Tail = nowy;
}

void Karta::Tasuj_Talie(Karta * &Head, Karta * &Tail)
{
	srand(time(nullptr));
	Karta *tasowana=Head;
	Karta *tmp;
	int losowane;

	for (int i = 0; i < 1000; i++)
	{
		tasowana = Head;
		losowane = rand()%52+1;
		
		for (int j=0;j<51;j++)
		{
			if (tasowana->id_karty == losowane)
				break;
			tasowana = tasowana->Next;
		}
		//sprawdzanie losowania
		//if (losowane==1)
		//cout << "|" << losowane << "|" << tasowana->id_karty << "\t|" << tasowana->wartosc_karty << "\t|" << tasowana->kolor_karty << "\t|" << tasowana->znak_karty << endl;
				
		if (tasowana==Head)
			continue;
		
		
		else if (tasowana == Tail)
		{
			//laczenie tali
			tmp = tasowana->Prev;
			tmp->Next = nullptr;
			Tail = tmp;

			//dodawanie na poczatek
			Head->Prev = tasowana;
			tasowana->Next = Head;
			Head = tasowana;
			
		}
		else
		{
			//laczenie tali
			tmp = tasowana->Prev;
			tmp->Next = tasowana->Next;
			tmp = tasowana->Next;
			tmp->Prev = tasowana->Prev;

			//dodawanie na poczatek
			Head->Prev = tasowana;
			tasowana->Next = Head;
			tasowana->Prev = nullptr;
			Head = tasowana;
		}		
	}
}
void Karta::WyswietlTalie(Karta* &Head)
{
	Karta * Current = Head;
	while (Current != nullptr)
	{
		cout << Current->id_karty<<endl;
		Current = Current->Next;
	}
}
void Karta::WyswietlTalieOdKonca(Karta* &Tail)
{
	Karta * Current = Tail;
	for (int i = 0; i < 51;i++)
	{
		cout << Current->id_karty << endl;
		Current = Current->Prev;
	}
	
}

